-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sat Sep 19 14:08:52 2020
-- Host        : clevo running 64-bit Debian GNU/Linux 9.13 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/harlock/Documents/Xilinx_projects/LES5_AXI_DMA_BLOCK_DESIGN/LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/design_1_stub.vhdl
-- Design      : design_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1 is
  Port ( 
    AXI_IN_CLK : in STD_LOGIC;
    AXI_TIMER_interrupt_OUT_PORT : out STD_LOGIC;
    M03_AXI_master2SLAVE_OUT_PORT_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_bid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_wlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_master2SLAVE_OUT_PORT_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_master2SLAVE_OUT_PORT_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arready : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_arvalid : in STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awready : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_awvalid : in STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_bready : in STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_bvalid : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S01_AXI_MASTER2slave_IN_PORT_rlast : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_rready : in STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_rvalid : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_wlast : in STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_wready : out STD_LOGIC;
    S01_AXI_MASTER2slave_IN_PORT_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S01_AXI_MASTER2slave_IN_PORT_wvalid : in STD_LOGIC;
    UART_OUT_PORT_rxd : in STD_LOGIC;
    UART_OUT_PORT_txd : out STD_LOGIC;
    UART_interrupt_OUT_PORT : out STD_LOGIC;
    cdma_interrupt_out_PORT : out STD_LOGIC;
    dcm_locked_IN_PORT : in STD_LOGIC;
    reset_rtl : in STD_LOGIC
  );

end design_1;

architecture stub of design_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "AXI_IN_CLK,AXI_TIMER_interrupt_OUT_PORT,M03_AXI_master2SLAVE_OUT_PORT_araddr[31:0],M03_AXI_master2SLAVE_OUT_PORT_arburst[1:0],M03_AXI_master2SLAVE_OUT_PORT_arcache[3:0],M03_AXI_master2SLAVE_OUT_PORT_arid[1:0],M03_AXI_master2SLAVE_OUT_PORT_arlen[7:0],M03_AXI_master2SLAVE_OUT_PORT_arlock[0:0],M03_AXI_master2SLAVE_OUT_PORT_arprot[2:0],M03_AXI_master2SLAVE_OUT_PORT_arqos[3:0],M03_AXI_master2SLAVE_OUT_PORT_arready[0:0],M03_AXI_master2SLAVE_OUT_PORT_arregion[3:0],M03_AXI_master2SLAVE_OUT_PORT_arsize[2:0],M03_AXI_master2SLAVE_OUT_PORT_arvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_awaddr[31:0],M03_AXI_master2SLAVE_OUT_PORT_awburst[1:0],M03_AXI_master2SLAVE_OUT_PORT_awcache[3:0],M03_AXI_master2SLAVE_OUT_PORT_awid[1:0],M03_AXI_master2SLAVE_OUT_PORT_awlen[7:0],M03_AXI_master2SLAVE_OUT_PORT_awlock[0:0],M03_AXI_master2SLAVE_OUT_PORT_awprot[2:0],M03_AXI_master2SLAVE_OUT_PORT_awqos[3:0],M03_AXI_master2SLAVE_OUT_PORT_awready[0:0],M03_AXI_master2SLAVE_OUT_PORT_awregion[3:0],M03_AXI_master2SLAVE_OUT_PORT_awsize[2:0],M03_AXI_master2SLAVE_OUT_PORT_awvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_bid[1:0],M03_AXI_master2SLAVE_OUT_PORT_bready[0:0],M03_AXI_master2SLAVE_OUT_PORT_bresp[1:0],M03_AXI_master2SLAVE_OUT_PORT_bvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_rdata[31:0],M03_AXI_master2SLAVE_OUT_PORT_rid[1:0],M03_AXI_master2SLAVE_OUT_PORT_rlast[0:0],M03_AXI_master2SLAVE_OUT_PORT_rready[0:0],M03_AXI_master2SLAVE_OUT_PORT_rresp[1:0],M03_AXI_master2SLAVE_OUT_PORT_rvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_wdata[31:0],M03_AXI_master2SLAVE_OUT_PORT_wlast[0:0],M03_AXI_master2SLAVE_OUT_PORT_wready[0:0],M03_AXI_master2SLAVE_OUT_PORT_wstrb[3:0],M03_AXI_master2SLAVE_OUT_PORT_wvalid[0:0],S01_AXI_MASTER2slave_IN_PORT_araddr[31:0],S01_AXI_MASTER2slave_IN_PORT_arburst[1:0],S01_AXI_MASTER2slave_IN_PORT_arcache[3:0],S01_AXI_MASTER2slave_IN_PORT_arid[0:0],S01_AXI_MASTER2slave_IN_PORT_arlen[7:0],S01_AXI_MASTER2slave_IN_PORT_arlock[0:0],S01_AXI_MASTER2slave_IN_PORT_arprot[2:0],S01_AXI_MASTER2slave_IN_PORT_arqos[3:0],S01_AXI_MASTER2slave_IN_PORT_arready,S01_AXI_MASTER2slave_IN_PORT_arregion[3:0],S01_AXI_MASTER2slave_IN_PORT_arsize[2:0],S01_AXI_MASTER2slave_IN_PORT_arvalid,S01_AXI_MASTER2slave_IN_PORT_awaddr[31:0],S01_AXI_MASTER2slave_IN_PORT_awburst[1:0],S01_AXI_MASTER2slave_IN_PORT_awcache[3:0],S01_AXI_MASTER2slave_IN_PORT_awid[0:0],S01_AXI_MASTER2slave_IN_PORT_awlen[7:0],S01_AXI_MASTER2slave_IN_PORT_awlock[0:0],S01_AXI_MASTER2slave_IN_PORT_awprot[2:0],S01_AXI_MASTER2slave_IN_PORT_awqos[3:0],S01_AXI_MASTER2slave_IN_PORT_awready,S01_AXI_MASTER2slave_IN_PORT_awregion[3:0],S01_AXI_MASTER2slave_IN_PORT_awsize[2:0],S01_AXI_MASTER2slave_IN_PORT_awvalid,S01_AXI_MASTER2slave_IN_PORT_bid[0:0],S01_AXI_MASTER2slave_IN_PORT_bready,S01_AXI_MASTER2slave_IN_PORT_bresp[1:0],S01_AXI_MASTER2slave_IN_PORT_bvalid,S01_AXI_MASTER2slave_IN_PORT_rdata[31:0],S01_AXI_MASTER2slave_IN_PORT_rid[0:0],S01_AXI_MASTER2slave_IN_PORT_rlast,S01_AXI_MASTER2slave_IN_PORT_rready,S01_AXI_MASTER2slave_IN_PORT_rresp[1:0],S01_AXI_MASTER2slave_IN_PORT_rvalid,S01_AXI_MASTER2slave_IN_PORT_wdata[31:0],S01_AXI_MASTER2slave_IN_PORT_wlast,S01_AXI_MASTER2slave_IN_PORT_wready,S01_AXI_MASTER2slave_IN_PORT_wstrb[3:0],S01_AXI_MASTER2slave_IN_PORT_wvalid,UART_OUT_PORT_rxd,UART_OUT_PORT_txd,UART_interrupt_OUT_PORT,cdma_interrupt_out_PORT,dcm_locked_IN_PORT,reset_rtl";
begin
end;
