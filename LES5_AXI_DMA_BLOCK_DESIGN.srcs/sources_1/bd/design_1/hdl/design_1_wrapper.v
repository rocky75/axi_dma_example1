//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (lin64) Build 932637 Wed Jun 11 13:08:52 MDT 2014
//Date        : Tue Sep 15 13:44:02 2020
//Host        : clevo running 64-bit Debian GNU/Linux 9.13 (stretch)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (AXI_INTERCONNECT_RESETN_IN_PORT,
    AXI_IN_CLK,
    AXI_PERIPHERAL_ARESETN_IN_PORT,
    AXI_TIMER_interrupt_OUT_PORT,
    M03_AXI_master2SLAVE_OUT_PORT_araddr,
    M03_AXI_master2SLAVE_OUT_PORT_arburst,
    M03_AXI_master2SLAVE_OUT_PORT_arcache,
    M03_AXI_master2SLAVE_OUT_PORT_arid,
    M03_AXI_master2SLAVE_OUT_PORT_arlen,
    M03_AXI_master2SLAVE_OUT_PORT_arlock,
    M03_AXI_master2SLAVE_OUT_PORT_arprot,
    M03_AXI_master2SLAVE_OUT_PORT_arqos,
    M03_AXI_master2SLAVE_OUT_PORT_arready,
    M03_AXI_master2SLAVE_OUT_PORT_arregion,
    M03_AXI_master2SLAVE_OUT_PORT_arsize,
    M03_AXI_master2SLAVE_OUT_PORT_arvalid,
    M03_AXI_master2SLAVE_OUT_PORT_awaddr,
    M03_AXI_master2SLAVE_OUT_PORT_awburst,
    M03_AXI_master2SLAVE_OUT_PORT_awcache,
    M03_AXI_master2SLAVE_OUT_PORT_awid,
    M03_AXI_master2SLAVE_OUT_PORT_awlen,
    M03_AXI_master2SLAVE_OUT_PORT_awlock,
    M03_AXI_master2SLAVE_OUT_PORT_awprot,
    M03_AXI_master2SLAVE_OUT_PORT_awqos,
    M03_AXI_master2SLAVE_OUT_PORT_awready,
    M03_AXI_master2SLAVE_OUT_PORT_awregion,
    M03_AXI_master2SLAVE_OUT_PORT_awsize,
    M03_AXI_master2SLAVE_OUT_PORT_awvalid,
    M03_AXI_master2SLAVE_OUT_PORT_bid,
    M03_AXI_master2SLAVE_OUT_PORT_bready,
    M03_AXI_master2SLAVE_OUT_PORT_bresp,
    M03_AXI_master2SLAVE_OUT_PORT_bvalid,
    M03_AXI_master2SLAVE_OUT_PORT_rdata,
    M03_AXI_master2SLAVE_OUT_PORT_rid,
    M03_AXI_master2SLAVE_OUT_PORT_rlast,
    M03_AXI_master2SLAVE_OUT_PORT_rready,
    M03_AXI_master2SLAVE_OUT_PORT_rresp,
    M03_AXI_master2SLAVE_OUT_PORT_rvalid,
    M03_AXI_master2SLAVE_OUT_PORT_wdata,
    M03_AXI_master2SLAVE_OUT_PORT_wlast,
    M03_AXI_master2SLAVE_OUT_PORT_wready,
    M03_AXI_master2SLAVE_OUT_PORT_wstrb,
    M03_AXI_master2SLAVE_OUT_PORT_wvalid,
    S01_AXI_MASTER2slave_IN_PORT_araddr,
    S01_AXI_MASTER2slave_IN_PORT_arburst,
    S01_AXI_MASTER2slave_IN_PORT_arcache,
    S01_AXI_MASTER2slave_IN_PORT_arid,
    S01_AXI_MASTER2slave_IN_PORT_arlen,
    S01_AXI_MASTER2slave_IN_PORT_arlock,
    S01_AXI_MASTER2slave_IN_PORT_arprot,
    S01_AXI_MASTER2slave_IN_PORT_arqos,
    S01_AXI_MASTER2slave_IN_PORT_arready,
    S01_AXI_MASTER2slave_IN_PORT_arsize,
    S01_AXI_MASTER2slave_IN_PORT_arvalid,
    S01_AXI_MASTER2slave_IN_PORT_awaddr,
    S01_AXI_MASTER2slave_IN_PORT_awburst,
    S01_AXI_MASTER2slave_IN_PORT_awcache,
    S01_AXI_MASTER2slave_IN_PORT_awid,
    S01_AXI_MASTER2slave_IN_PORT_awlen,
    S01_AXI_MASTER2slave_IN_PORT_awlock,
    S01_AXI_MASTER2slave_IN_PORT_awprot,
    S01_AXI_MASTER2slave_IN_PORT_awqos,
    S01_AXI_MASTER2slave_IN_PORT_awready,
    S01_AXI_MASTER2slave_IN_PORT_awsize,
    S01_AXI_MASTER2slave_IN_PORT_awvalid,
    S01_AXI_MASTER2slave_IN_PORT_bid,
    S01_AXI_MASTER2slave_IN_PORT_bready,
    S01_AXI_MASTER2slave_IN_PORT_bresp,
    S01_AXI_MASTER2slave_IN_PORT_bvalid,
    S01_AXI_MASTER2slave_IN_PORT_rdata,
    S01_AXI_MASTER2slave_IN_PORT_rid,
    S01_AXI_MASTER2slave_IN_PORT_rlast,
    S01_AXI_MASTER2slave_IN_PORT_rready,
    S01_AXI_MASTER2slave_IN_PORT_rresp,
    S01_AXI_MASTER2slave_IN_PORT_rvalid,
    S01_AXI_MASTER2slave_IN_PORT_wdata,
    S01_AXI_MASTER2slave_IN_PORT_wlast,
    S01_AXI_MASTER2slave_IN_PORT_wready,
    S01_AXI_MASTER2slave_IN_PORT_wstrb,
    S01_AXI_MASTER2slave_IN_PORT_wvalid,
    UART_OUT_PORT_rxd,
    UART_OUT_PORT_txd,
    UART_interrupt_OUT_PORT,
    cdma_interrupt_out_PORT);
  input AXI_INTERCONNECT_RESETN_IN_PORT;
  input AXI_IN_CLK;
  input AXI_PERIPHERAL_ARESETN_IN_PORT;
  output AXI_TIMER_interrupt_OUT_PORT;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_araddr;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_arburst;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arcache;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_arid;
  output [7:0]M03_AXI_master2SLAVE_OUT_PORT_arlen;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_arlock;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_arprot;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arqos;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_arready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arregion;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_arsize;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_arvalid;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_awaddr;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_awburst;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awcache;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_awid;
  output [7:0]M03_AXI_master2SLAVE_OUT_PORT_awlen;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_awlock;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_awprot;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awqos;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_awready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awregion;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_awsize;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_awvalid;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_bid;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_bready;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_bresp;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_bvalid;
  input [31:0]M03_AXI_master2SLAVE_OUT_PORT_rdata;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_rid;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_rlast;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_rready;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_rresp;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_rvalid;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_wdata;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_wlast;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_wready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_wstrb;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_wvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_araddr;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_arburst;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_arcache;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_arid;
  input [7:0]S01_AXI_MASTER2slave_IN_PORT_arlen;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_arlock;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_arprot;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_arqos;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_arready;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_arsize;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_arvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_awaddr;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_awburst;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_awcache;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_awid;
  input [7:0]S01_AXI_MASTER2slave_IN_PORT_awlen;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_awlock;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_awprot;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_awqos;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_awready;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_awsize;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_awvalid;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_bid;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_bready;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_bresp;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_bvalid;
  output [31:0]S01_AXI_MASTER2slave_IN_PORT_rdata;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_rid;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_rlast;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_rready;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_rresp;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_rvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_wdata;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_wlast;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_wready;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_wstrb;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_wvalid;
  input UART_OUT_PORT_rxd;
  output UART_OUT_PORT_txd;
  output UART_interrupt_OUT_PORT;
  output cdma_interrupt_out_PORT;

  wire AXI_INTERCONNECT_RESETN_IN_PORT;
  wire AXI_IN_CLK;
  wire AXI_PERIPHERAL_ARESETN_IN_PORT;
  wire AXI_TIMER_interrupt_OUT_PORT;
  wire [31:0]M03_AXI_master2SLAVE_OUT_PORT_araddr;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_arburst;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_arcache;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_arid;
  wire [7:0]M03_AXI_master2SLAVE_OUT_PORT_arlen;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_arlock;
  wire [2:0]M03_AXI_master2SLAVE_OUT_PORT_arprot;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_arqos;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_arready;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_arregion;
  wire [2:0]M03_AXI_master2SLAVE_OUT_PORT_arsize;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_arvalid;
  wire [31:0]M03_AXI_master2SLAVE_OUT_PORT_awaddr;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_awburst;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_awcache;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_awid;
  wire [7:0]M03_AXI_master2SLAVE_OUT_PORT_awlen;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_awlock;
  wire [2:0]M03_AXI_master2SLAVE_OUT_PORT_awprot;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_awqos;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_awready;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_awregion;
  wire [2:0]M03_AXI_master2SLAVE_OUT_PORT_awsize;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_awvalid;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_bid;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_bready;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_bresp;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_bvalid;
  wire [31:0]M03_AXI_master2SLAVE_OUT_PORT_rdata;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_rid;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_rlast;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_rready;
  wire [1:0]M03_AXI_master2SLAVE_OUT_PORT_rresp;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_rvalid;
  wire [31:0]M03_AXI_master2SLAVE_OUT_PORT_wdata;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_wlast;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_wready;
  wire [3:0]M03_AXI_master2SLAVE_OUT_PORT_wstrb;
  wire [0:0]M03_AXI_master2SLAVE_OUT_PORT_wvalid;
  wire [31:0]S01_AXI_MASTER2slave_IN_PORT_araddr;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_arburst;
  wire [3:0]S01_AXI_MASTER2slave_IN_PORT_arcache;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_arid;
  wire [7:0]S01_AXI_MASTER2slave_IN_PORT_arlen;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_arlock;
  wire [2:0]S01_AXI_MASTER2slave_IN_PORT_arprot;
  wire [3:0]S01_AXI_MASTER2slave_IN_PORT_arqos;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_arready;
  wire [2:0]S01_AXI_MASTER2slave_IN_PORT_arsize;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_arvalid;
  wire [31:0]S01_AXI_MASTER2slave_IN_PORT_awaddr;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_awburst;
  wire [3:0]S01_AXI_MASTER2slave_IN_PORT_awcache;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_awid;
  wire [7:0]S01_AXI_MASTER2slave_IN_PORT_awlen;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_awlock;
  wire [2:0]S01_AXI_MASTER2slave_IN_PORT_awprot;
  wire [3:0]S01_AXI_MASTER2slave_IN_PORT_awqos;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_awready;
  wire [2:0]S01_AXI_MASTER2slave_IN_PORT_awsize;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_awvalid;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_bid;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_bready;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_bresp;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_bvalid;
  wire [31:0]S01_AXI_MASTER2slave_IN_PORT_rdata;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_rid;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_rlast;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_rready;
  wire [1:0]S01_AXI_MASTER2slave_IN_PORT_rresp;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_rvalid;
  wire [31:0]S01_AXI_MASTER2slave_IN_PORT_wdata;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_wlast;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_wready;
  wire [3:0]S01_AXI_MASTER2slave_IN_PORT_wstrb;
  wire [0:0]S01_AXI_MASTER2slave_IN_PORT_wvalid;
  wire UART_OUT_PORT_rxd;
  wire UART_OUT_PORT_txd;
  wire UART_interrupt_OUT_PORT;
  wire cdma_interrupt_out_PORT;

design_1 design_1_i
       (.AXI_INTERCONNECT_RESETN_IN_PORT(AXI_INTERCONNECT_RESETN_IN_PORT),
        .AXI_IN_CLK(AXI_IN_CLK),
        .AXI_PERIPHERAL_ARESETN_IN_PORT(AXI_PERIPHERAL_ARESETN_IN_PORT),
        .AXI_TIMER_interrupt_OUT_PORT(AXI_TIMER_interrupt_OUT_PORT),
        .M03_AXI_master2SLAVE_OUT_PORT_araddr(M03_AXI_master2SLAVE_OUT_PORT_araddr),
        .M03_AXI_master2SLAVE_OUT_PORT_arburst(M03_AXI_master2SLAVE_OUT_PORT_arburst),
        .M03_AXI_master2SLAVE_OUT_PORT_arcache(M03_AXI_master2SLAVE_OUT_PORT_arcache),
        .M03_AXI_master2SLAVE_OUT_PORT_arid(M03_AXI_master2SLAVE_OUT_PORT_arid),
        .M03_AXI_master2SLAVE_OUT_PORT_arlen(M03_AXI_master2SLAVE_OUT_PORT_arlen),
        .M03_AXI_master2SLAVE_OUT_PORT_arlock(M03_AXI_master2SLAVE_OUT_PORT_arlock),
        .M03_AXI_master2SLAVE_OUT_PORT_arprot(M03_AXI_master2SLAVE_OUT_PORT_arprot),
        .M03_AXI_master2SLAVE_OUT_PORT_arqos(M03_AXI_master2SLAVE_OUT_PORT_arqos),
        .M03_AXI_master2SLAVE_OUT_PORT_arready(M03_AXI_master2SLAVE_OUT_PORT_arready),
        .M03_AXI_master2SLAVE_OUT_PORT_arregion(M03_AXI_master2SLAVE_OUT_PORT_arregion),
        .M03_AXI_master2SLAVE_OUT_PORT_arsize(M03_AXI_master2SLAVE_OUT_PORT_arsize),
        .M03_AXI_master2SLAVE_OUT_PORT_arvalid(M03_AXI_master2SLAVE_OUT_PORT_arvalid),
        .M03_AXI_master2SLAVE_OUT_PORT_awaddr(M03_AXI_master2SLAVE_OUT_PORT_awaddr),
        .M03_AXI_master2SLAVE_OUT_PORT_awburst(M03_AXI_master2SLAVE_OUT_PORT_awburst),
        .M03_AXI_master2SLAVE_OUT_PORT_awcache(M03_AXI_master2SLAVE_OUT_PORT_awcache),
        .M03_AXI_master2SLAVE_OUT_PORT_awid(M03_AXI_master2SLAVE_OUT_PORT_awid),
        .M03_AXI_master2SLAVE_OUT_PORT_awlen(M03_AXI_master2SLAVE_OUT_PORT_awlen),
        .M03_AXI_master2SLAVE_OUT_PORT_awlock(M03_AXI_master2SLAVE_OUT_PORT_awlock),
        .M03_AXI_master2SLAVE_OUT_PORT_awprot(M03_AXI_master2SLAVE_OUT_PORT_awprot),
        .M03_AXI_master2SLAVE_OUT_PORT_awqos(M03_AXI_master2SLAVE_OUT_PORT_awqos),
        .M03_AXI_master2SLAVE_OUT_PORT_awready(M03_AXI_master2SLAVE_OUT_PORT_awready),
        .M03_AXI_master2SLAVE_OUT_PORT_awregion(M03_AXI_master2SLAVE_OUT_PORT_awregion),
        .M03_AXI_master2SLAVE_OUT_PORT_awsize(M03_AXI_master2SLAVE_OUT_PORT_awsize),
        .M03_AXI_master2SLAVE_OUT_PORT_awvalid(M03_AXI_master2SLAVE_OUT_PORT_awvalid),
        .M03_AXI_master2SLAVE_OUT_PORT_bid(M03_AXI_master2SLAVE_OUT_PORT_bid),
        .M03_AXI_master2SLAVE_OUT_PORT_bready(M03_AXI_master2SLAVE_OUT_PORT_bready),
        .M03_AXI_master2SLAVE_OUT_PORT_bresp(M03_AXI_master2SLAVE_OUT_PORT_bresp),
        .M03_AXI_master2SLAVE_OUT_PORT_bvalid(M03_AXI_master2SLAVE_OUT_PORT_bvalid),
        .M03_AXI_master2SLAVE_OUT_PORT_rdata(M03_AXI_master2SLAVE_OUT_PORT_rdata),
        .M03_AXI_master2SLAVE_OUT_PORT_rid(M03_AXI_master2SLAVE_OUT_PORT_rid),
        .M03_AXI_master2SLAVE_OUT_PORT_rlast(M03_AXI_master2SLAVE_OUT_PORT_rlast),
        .M03_AXI_master2SLAVE_OUT_PORT_rready(M03_AXI_master2SLAVE_OUT_PORT_rready),
        .M03_AXI_master2SLAVE_OUT_PORT_rresp(M03_AXI_master2SLAVE_OUT_PORT_rresp),
        .M03_AXI_master2SLAVE_OUT_PORT_rvalid(M03_AXI_master2SLAVE_OUT_PORT_rvalid),
        .M03_AXI_master2SLAVE_OUT_PORT_wdata(M03_AXI_master2SLAVE_OUT_PORT_wdata),
        .M03_AXI_master2SLAVE_OUT_PORT_wlast(M03_AXI_master2SLAVE_OUT_PORT_wlast),
        .M03_AXI_master2SLAVE_OUT_PORT_wready(M03_AXI_master2SLAVE_OUT_PORT_wready),
        .M03_AXI_master2SLAVE_OUT_PORT_wstrb(M03_AXI_master2SLAVE_OUT_PORT_wstrb),
        .M03_AXI_master2SLAVE_OUT_PORT_wvalid(M03_AXI_master2SLAVE_OUT_PORT_wvalid),
        .S01_AXI_MASTER2slave_IN_PORT_araddr(S01_AXI_MASTER2slave_IN_PORT_araddr),
        .S01_AXI_MASTER2slave_IN_PORT_arburst(S01_AXI_MASTER2slave_IN_PORT_arburst),
        .S01_AXI_MASTER2slave_IN_PORT_arcache(S01_AXI_MASTER2slave_IN_PORT_arcache),
        .S01_AXI_MASTER2slave_IN_PORT_arid(S01_AXI_MASTER2slave_IN_PORT_arid),
        .S01_AXI_MASTER2slave_IN_PORT_arlen(S01_AXI_MASTER2slave_IN_PORT_arlen),
        .S01_AXI_MASTER2slave_IN_PORT_arlock(S01_AXI_MASTER2slave_IN_PORT_arlock),
        .S01_AXI_MASTER2slave_IN_PORT_arprot(S01_AXI_MASTER2slave_IN_PORT_arprot),
        .S01_AXI_MASTER2slave_IN_PORT_arqos(S01_AXI_MASTER2slave_IN_PORT_arqos),
        .S01_AXI_MASTER2slave_IN_PORT_arready(S01_AXI_MASTER2slave_IN_PORT_arready),
        .S01_AXI_MASTER2slave_IN_PORT_arsize(S01_AXI_MASTER2slave_IN_PORT_arsize),
        .S01_AXI_MASTER2slave_IN_PORT_arvalid(S01_AXI_MASTER2slave_IN_PORT_arvalid),
        .S01_AXI_MASTER2slave_IN_PORT_awaddr(S01_AXI_MASTER2slave_IN_PORT_awaddr),
        .S01_AXI_MASTER2slave_IN_PORT_awburst(S01_AXI_MASTER2slave_IN_PORT_awburst),
        .S01_AXI_MASTER2slave_IN_PORT_awcache(S01_AXI_MASTER2slave_IN_PORT_awcache),
        .S01_AXI_MASTER2slave_IN_PORT_awid(S01_AXI_MASTER2slave_IN_PORT_awid),
        .S01_AXI_MASTER2slave_IN_PORT_awlen(S01_AXI_MASTER2slave_IN_PORT_awlen),
        .S01_AXI_MASTER2slave_IN_PORT_awlock(S01_AXI_MASTER2slave_IN_PORT_awlock),
        .S01_AXI_MASTER2slave_IN_PORT_awprot(S01_AXI_MASTER2slave_IN_PORT_awprot),
        .S01_AXI_MASTER2slave_IN_PORT_awqos(S01_AXI_MASTER2slave_IN_PORT_awqos),
        .S01_AXI_MASTER2slave_IN_PORT_awready(S01_AXI_MASTER2slave_IN_PORT_awready),
        .S01_AXI_MASTER2slave_IN_PORT_awsize(S01_AXI_MASTER2slave_IN_PORT_awsize),
        .S01_AXI_MASTER2slave_IN_PORT_awvalid(S01_AXI_MASTER2slave_IN_PORT_awvalid),
        .S01_AXI_MASTER2slave_IN_PORT_bid(S01_AXI_MASTER2slave_IN_PORT_bid),
        .S01_AXI_MASTER2slave_IN_PORT_bready(S01_AXI_MASTER2slave_IN_PORT_bready),
        .S01_AXI_MASTER2slave_IN_PORT_bresp(S01_AXI_MASTER2slave_IN_PORT_bresp),
        .S01_AXI_MASTER2slave_IN_PORT_bvalid(S01_AXI_MASTER2slave_IN_PORT_bvalid),
        .S01_AXI_MASTER2slave_IN_PORT_rdata(S01_AXI_MASTER2slave_IN_PORT_rdata),
        .S01_AXI_MASTER2slave_IN_PORT_rid(S01_AXI_MASTER2slave_IN_PORT_rid),
        .S01_AXI_MASTER2slave_IN_PORT_rlast(S01_AXI_MASTER2slave_IN_PORT_rlast),
        .S01_AXI_MASTER2slave_IN_PORT_rready(S01_AXI_MASTER2slave_IN_PORT_rready),
        .S01_AXI_MASTER2slave_IN_PORT_rresp(S01_AXI_MASTER2slave_IN_PORT_rresp),
        .S01_AXI_MASTER2slave_IN_PORT_rvalid(S01_AXI_MASTER2slave_IN_PORT_rvalid),
        .S01_AXI_MASTER2slave_IN_PORT_wdata(S01_AXI_MASTER2slave_IN_PORT_wdata),
        .S01_AXI_MASTER2slave_IN_PORT_wlast(S01_AXI_MASTER2slave_IN_PORT_wlast),
        .S01_AXI_MASTER2slave_IN_PORT_wready(S01_AXI_MASTER2slave_IN_PORT_wready),
        .S01_AXI_MASTER2slave_IN_PORT_wstrb(S01_AXI_MASTER2slave_IN_PORT_wstrb),
        .S01_AXI_MASTER2slave_IN_PORT_wvalid(S01_AXI_MASTER2slave_IN_PORT_wvalid),
        .UART_OUT_PORT_rxd(UART_OUT_PORT_rxd),
        .UART_OUT_PORT_txd(UART_OUT_PORT_txd),
        .UART_interrupt_OUT_PORT(UART_interrupt_OUT_PORT),
        .cdma_interrupt_out_PORT(cdma_interrupt_out_PORT));
endmodule
