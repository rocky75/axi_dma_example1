// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sat Sep 19 14:08:52 2020
// Host        : clevo running 64-bit Debian GNU/Linux 9.13 (stretch)
// Command     : write_verilog -force -mode synth_stub
//               /home/harlock/Documents/Xilinx_projects/LES5_AXI_DMA_BLOCK_DESIGN/LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/design_1_stub.v
// Design      : design_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module design_1(AXI_IN_CLK, AXI_TIMER_interrupt_OUT_PORT, 
  M03_AXI_master2SLAVE_OUT_PORT_araddr, M03_AXI_master2SLAVE_OUT_PORT_arburst, 
  M03_AXI_master2SLAVE_OUT_PORT_arcache, M03_AXI_master2SLAVE_OUT_PORT_arid, 
  M03_AXI_master2SLAVE_OUT_PORT_arlen, M03_AXI_master2SLAVE_OUT_PORT_arlock, 
  M03_AXI_master2SLAVE_OUT_PORT_arprot, M03_AXI_master2SLAVE_OUT_PORT_arqos, 
  M03_AXI_master2SLAVE_OUT_PORT_arready, M03_AXI_master2SLAVE_OUT_PORT_arregion, 
  M03_AXI_master2SLAVE_OUT_PORT_arsize, M03_AXI_master2SLAVE_OUT_PORT_arvalid, 
  M03_AXI_master2SLAVE_OUT_PORT_awaddr, M03_AXI_master2SLAVE_OUT_PORT_awburst, 
  M03_AXI_master2SLAVE_OUT_PORT_awcache, M03_AXI_master2SLAVE_OUT_PORT_awid, 
  M03_AXI_master2SLAVE_OUT_PORT_awlen, M03_AXI_master2SLAVE_OUT_PORT_awlock, 
  M03_AXI_master2SLAVE_OUT_PORT_awprot, M03_AXI_master2SLAVE_OUT_PORT_awqos, 
  M03_AXI_master2SLAVE_OUT_PORT_awready, M03_AXI_master2SLAVE_OUT_PORT_awregion, 
  M03_AXI_master2SLAVE_OUT_PORT_awsize, M03_AXI_master2SLAVE_OUT_PORT_awvalid, 
  M03_AXI_master2SLAVE_OUT_PORT_bid, M03_AXI_master2SLAVE_OUT_PORT_bready, 
  M03_AXI_master2SLAVE_OUT_PORT_bresp, M03_AXI_master2SLAVE_OUT_PORT_bvalid, 
  M03_AXI_master2SLAVE_OUT_PORT_rdata, M03_AXI_master2SLAVE_OUT_PORT_rid, 
  M03_AXI_master2SLAVE_OUT_PORT_rlast, M03_AXI_master2SLAVE_OUT_PORT_rready, 
  M03_AXI_master2SLAVE_OUT_PORT_rresp, M03_AXI_master2SLAVE_OUT_PORT_rvalid, 
  M03_AXI_master2SLAVE_OUT_PORT_wdata, M03_AXI_master2SLAVE_OUT_PORT_wlast, 
  M03_AXI_master2SLAVE_OUT_PORT_wready, M03_AXI_master2SLAVE_OUT_PORT_wstrb, 
  M03_AXI_master2SLAVE_OUT_PORT_wvalid, S01_AXI_MASTER2slave_IN_PORT_araddr, 
  S01_AXI_MASTER2slave_IN_PORT_arburst, S01_AXI_MASTER2slave_IN_PORT_arcache, 
  S01_AXI_MASTER2slave_IN_PORT_arid, S01_AXI_MASTER2slave_IN_PORT_arlen, 
  S01_AXI_MASTER2slave_IN_PORT_arlock, S01_AXI_MASTER2slave_IN_PORT_arprot, 
  S01_AXI_MASTER2slave_IN_PORT_arqos, S01_AXI_MASTER2slave_IN_PORT_arready, 
  S01_AXI_MASTER2slave_IN_PORT_arregion, S01_AXI_MASTER2slave_IN_PORT_arsize, 
  S01_AXI_MASTER2slave_IN_PORT_arvalid, S01_AXI_MASTER2slave_IN_PORT_awaddr, 
  S01_AXI_MASTER2slave_IN_PORT_awburst, S01_AXI_MASTER2slave_IN_PORT_awcache, 
  S01_AXI_MASTER2slave_IN_PORT_awid, S01_AXI_MASTER2slave_IN_PORT_awlen, 
  S01_AXI_MASTER2slave_IN_PORT_awlock, S01_AXI_MASTER2slave_IN_PORT_awprot, 
  S01_AXI_MASTER2slave_IN_PORT_awqos, S01_AXI_MASTER2slave_IN_PORT_awready, 
  S01_AXI_MASTER2slave_IN_PORT_awregion, S01_AXI_MASTER2slave_IN_PORT_awsize, 
  S01_AXI_MASTER2slave_IN_PORT_awvalid, S01_AXI_MASTER2slave_IN_PORT_bid, 
  S01_AXI_MASTER2slave_IN_PORT_bready, S01_AXI_MASTER2slave_IN_PORT_bresp, 
  S01_AXI_MASTER2slave_IN_PORT_bvalid, S01_AXI_MASTER2slave_IN_PORT_rdata, 
  S01_AXI_MASTER2slave_IN_PORT_rid, S01_AXI_MASTER2slave_IN_PORT_rlast, 
  S01_AXI_MASTER2slave_IN_PORT_rready, S01_AXI_MASTER2slave_IN_PORT_rresp, 
  S01_AXI_MASTER2slave_IN_PORT_rvalid, S01_AXI_MASTER2slave_IN_PORT_wdata, 
  S01_AXI_MASTER2slave_IN_PORT_wlast, S01_AXI_MASTER2slave_IN_PORT_wready, 
  S01_AXI_MASTER2slave_IN_PORT_wstrb, S01_AXI_MASTER2slave_IN_PORT_wvalid, 
  UART_OUT_PORT_rxd, UART_OUT_PORT_txd, UART_interrupt_OUT_PORT, cdma_interrupt_out_PORT, 
  dcm_locked_IN_PORT, reset_rtl)
/* synthesis syn_black_box black_box_pad_pin="AXI_IN_CLK,AXI_TIMER_interrupt_OUT_PORT,M03_AXI_master2SLAVE_OUT_PORT_araddr[31:0],M03_AXI_master2SLAVE_OUT_PORT_arburst[1:0],M03_AXI_master2SLAVE_OUT_PORT_arcache[3:0],M03_AXI_master2SLAVE_OUT_PORT_arid[1:0],M03_AXI_master2SLAVE_OUT_PORT_arlen[7:0],M03_AXI_master2SLAVE_OUT_PORT_arlock[0:0],M03_AXI_master2SLAVE_OUT_PORT_arprot[2:0],M03_AXI_master2SLAVE_OUT_PORT_arqos[3:0],M03_AXI_master2SLAVE_OUT_PORT_arready[0:0],M03_AXI_master2SLAVE_OUT_PORT_arregion[3:0],M03_AXI_master2SLAVE_OUT_PORT_arsize[2:0],M03_AXI_master2SLAVE_OUT_PORT_arvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_awaddr[31:0],M03_AXI_master2SLAVE_OUT_PORT_awburst[1:0],M03_AXI_master2SLAVE_OUT_PORT_awcache[3:0],M03_AXI_master2SLAVE_OUT_PORT_awid[1:0],M03_AXI_master2SLAVE_OUT_PORT_awlen[7:0],M03_AXI_master2SLAVE_OUT_PORT_awlock[0:0],M03_AXI_master2SLAVE_OUT_PORT_awprot[2:0],M03_AXI_master2SLAVE_OUT_PORT_awqos[3:0],M03_AXI_master2SLAVE_OUT_PORT_awready[0:0],M03_AXI_master2SLAVE_OUT_PORT_awregion[3:0],M03_AXI_master2SLAVE_OUT_PORT_awsize[2:0],M03_AXI_master2SLAVE_OUT_PORT_awvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_bid[1:0],M03_AXI_master2SLAVE_OUT_PORT_bready[0:0],M03_AXI_master2SLAVE_OUT_PORT_bresp[1:0],M03_AXI_master2SLAVE_OUT_PORT_bvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_rdata[31:0],M03_AXI_master2SLAVE_OUT_PORT_rid[1:0],M03_AXI_master2SLAVE_OUT_PORT_rlast[0:0],M03_AXI_master2SLAVE_OUT_PORT_rready[0:0],M03_AXI_master2SLAVE_OUT_PORT_rresp[1:0],M03_AXI_master2SLAVE_OUT_PORT_rvalid[0:0],M03_AXI_master2SLAVE_OUT_PORT_wdata[31:0],M03_AXI_master2SLAVE_OUT_PORT_wlast[0:0],M03_AXI_master2SLAVE_OUT_PORT_wready[0:0],M03_AXI_master2SLAVE_OUT_PORT_wstrb[3:0],M03_AXI_master2SLAVE_OUT_PORT_wvalid[0:0],S01_AXI_MASTER2slave_IN_PORT_araddr[31:0],S01_AXI_MASTER2slave_IN_PORT_arburst[1:0],S01_AXI_MASTER2slave_IN_PORT_arcache[3:0],S01_AXI_MASTER2slave_IN_PORT_arid[0:0],S01_AXI_MASTER2slave_IN_PORT_arlen[7:0],S01_AXI_MASTER2slave_IN_PORT_arlock[0:0],S01_AXI_MASTER2slave_IN_PORT_arprot[2:0],S01_AXI_MASTER2slave_IN_PORT_arqos[3:0],S01_AXI_MASTER2slave_IN_PORT_arready,S01_AXI_MASTER2slave_IN_PORT_arregion[3:0],S01_AXI_MASTER2slave_IN_PORT_arsize[2:0],S01_AXI_MASTER2slave_IN_PORT_arvalid,S01_AXI_MASTER2slave_IN_PORT_awaddr[31:0],S01_AXI_MASTER2slave_IN_PORT_awburst[1:0],S01_AXI_MASTER2slave_IN_PORT_awcache[3:0],S01_AXI_MASTER2slave_IN_PORT_awid[0:0],S01_AXI_MASTER2slave_IN_PORT_awlen[7:0],S01_AXI_MASTER2slave_IN_PORT_awlock[0:0],S01_AXI_MASTER2slave_IN_PORT_awprot[2:0],S01_AXI_MASTER2slave_IN_PORT_awqos[3:0],S01_AXI_MASTER2slave_IN_PORT_awready,S01_AXI_MASTER2slave_IN_PORT_awregion[3:0],S01_AXI_MASTER2slave_IN_PORT_awsize[2:0],S01_AXI_MASTER2slave_IN_PORT_awvalid,S01_AXI_MASTER2slave_IN_PORT_bid[0:0],S01_AXI_MASTER2slave_IN_PORT_bready,S01_AXI_MASTER2slave_IN_PORT_bresp[1:0],S01_AXI_MASTER2slave_IN_PORT_bvalid,S01_AXI_MASTER2slave_IN_PORT_rdata[31:0],S01_AXI_MASTER2slave_IN_PORT_rid[0:0],S01_AXI_MASTER2slave_IN_PORT_rlast,S01_AXI_MASTER2slave_IN_PORT_rready,S01_AXI_MASTER2slave_IN_PORT_rresp[1:0],S01_AXI_MASTER2slave_IN_PORT_rvalid,S01_AXI_MASTER2slave_IN_PORT_wdata[31:0],S01_AXI_MASTER2slave_IN_PORT_wlast,S01_AXI_MASTER2slave_IN_PORT_wready,S01_AXI_MASTER2slave_IN_PORT_wstrb[3:0],S01_AXI_MASTER2slave_IN_PORT_wvalid,UART_OUT_PORT_rxd,UART_OUT_PORT_txd,UART_interrupt_OUT_PORT,cdma_interrupt_out_PORT,dcm_locked_IN_PORT,reset_rtl" */;
  input AXI_IN_CLK;
  output AXI_TIMER_interrupt_OUT_PORT;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_araddr;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_arburst;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arcache;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_arid;
  output [7:0]M03_AXI_master2SLAVE_OUT_PORT_arlen;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_arlock;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_arprot;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arqos;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_arready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_arregion;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_arsize;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_arvalid;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_awaddr;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_awburst;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awcache;
  output [1:0]M03_AXI_master2SLAVE_OUT_PORT_awid;
  output [7:0]M03_AXI_master2SLAVE_OUT_PORT_awlen;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_awlock;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_awprot;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awqos;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_awready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_awregion;
  output [2:0]M03_AXI_master2SLAVE_OUT_PORT_awsize;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_awvalid;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_bid;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_bready;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_bresp;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_bvalid;
  input [31:0]M03_AXI_master2SLAVE_OUT_PORT_rdata;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_rid;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_rlast;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_rready;
  input [1:0]M03_AXI_master2SLAVE_OUT_PORT_rresp;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_rvalid;
  output [31:0]M03_AXI_master2SLAVE_OUT_PORT_wdata;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_wlast;
  input [0:0]M03_AXI_master2SLAVE_OUT_PORT_wready;
  output [3:0]M03_AXI_master2SLAVE_OUT_PORT_wstrb;
  output [0:0]M03_AXI_master2SLAVE_OUT_PORT_wvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_araddr;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_arburst;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_arcache;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_arid;
  input [7:0]S01_AXI_MASTER2slave_IN_PORT_arlen;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_arlock;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_arprot;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_arqos;
  output S01_AXI_MASTER2slave_IN_PORT_arready;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_arregion;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_arsize;
  input S01_AXI_MASTER2slave_IN_PORT_arvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_awaddr;
  input [1:0]S01_AXI_MASTER2slave_IN_PORT_awburst;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_awcache;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_awid;
  input [7:0]S01_AXI_MASTER2slave_IN_PORT_awlen;
  input [0:0]S01_AXI_MASTER2slave_IN_PORT_awlock;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_awprot;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_awqos;
  output S01_AXI_MASTER2slave_IN_PORT_awready;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_awregion;
  input [2:0]S01_AXI_MASTER2slave_IN_PORT_awsize;
  input S01_AXI_MASTER2slave_IN_PORT_awvalid;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_bid;
  input S01_AXI_MASTER2slave_IN_PORT_bready;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_bresp;
  output S01_AXI_MASTER2slave_IN_PORT_bvalid;
  output [31:0]S01_AXI_MASTER2slave_IN_PORT_rdata;
  output [0:0]S01_AXI_MASTER2slave_IN_PORT_rid;
  output S01_AXI_MASTER2slave_IN_PORT_rlast;
  input S01_AXI_MASTER2slave_IN_PORT_rready;
  output [1:0]S01_AXI_MASTER2slave_IN_PORT_rresp;
  output S01_AXI_MASTER2slave_IN_PORT_rvalid;
  input [31:0]S01_AXI_MASTER2slave_IN_PORT_wdata;
  input S01_AXI_MASTER2slave_IN_PORT_wlast;
  output S01_AXI_MASTER2slave_IN_PORT_wready;
  input [3:0]S01_AXI_MASTER2slave_IN_PORT_wstrb;
  input S01_AXI_MASTER2slave_IN_PORT_wvalid;
  input UART_OUT_PORT_rxd;
  output UART_OUT_PORT_txd;
  output UART_interrupt_OUT_PORT;
  output cdma_interrupt_out_PORT;
  input dcm_locked_IN_PORT;
  input reset_rtl;
endmodule
