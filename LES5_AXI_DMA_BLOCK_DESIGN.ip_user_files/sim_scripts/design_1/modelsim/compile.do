vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xpm
vlib modelsim_lib/msim/axi_lite_ipif_v3_0_4
vlib modelsim_lib/msim/lib_cdc_v1_0_2
vlib modelsim_lib/msim/lib_pkg_v1_0_2
vlib modelsim_lib/msim/axi_timer_v2_0_19
vlib modelsim_lib/msim/lib_srl_fifo_v1_0_2
vlib modelsim_lib/msim/axi_uartlite_v2_0_21
vlib modelsim_lib/msim/blk_mem_gen_v8_3_6
vlib modelsim_lib/msim/axi_bram_ctrl_v4_0_14
vlib modelsim_lib/msim/blk_mem_gen_v8_4_1
vlib modelsim_lib/msim/fifo_generator_v13_2_2
vlib modelsim_lib/msim/lib_fifo_v1_0_11
vlib modelsim_lib/msim/axi_datamover_v5_1_19
vlib modelsim_lib/msim/axi_sg_v4_1_10
vlib modelsim_lib/msim/axi_cdma_v4_1_17
vlib modelsim_lib/msim/proc_sys_reset_v5_0_12
vlib modelsim_lib/msim/generic_baseblocks_v2_1_0
vlib modelsim_lib/msim/axi_infrastructure_v1_1_0
vlib modelsim_lib/msim/axi_register_slice_v2_1_17
vlib modelsim_lib/msim/axi_data_fifo_v2_1_16
vlib modelsim_lib/msim/axi_crossbar_v2_1_18
vlib modelsim_lib/msim/axi_protocol_converter_v2_1_17
vlib modelsim_lib/msim/axi_clock_converter_v2_1_16
vlib modelsim_lib/msim/axi_dwidth_converter_v2_1_17

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xpm modelsim_lib/msim/xpm
vmap axi_lite_ipif_v3_0_4 modelsim_lib/msim/axi_lite_ipif_v3_0_4
vmap lib_cdc_v1_0_2 modelsim_lib/msim/lib_cdc_v1_0_2
vmap lib_pkg_v1_0_2 modelsim_lib/msim/lib_pkg_v1_0_2
vmap axi_timer_v2_0_19 modelsim_lib/msim/axi_timer_v2_0_19
vmap lib_srl_fifo_v1_0_2 modelsim_lib/msim/lib_srl_fifo_v1_0_2
vmap axi_uartlite_v2_0_21 modelsim_lib/msim/axi_uartlite_v2_0_21
vmap blk_mem_gen_v8_3_6 modelsim_lib/msim/blk_mem_gen_v8_3_6
vmap axi_bram_ctrl_v4_0_14 modelsim_lib/msim/axi_bram_ctrl_v4_0_14
vmap blk_mem_gen_v8_4_1 modelsim_lib/msim/blk_mem_gen_v8_4_1
vmap fifo_generator_v13_2_2 modelsim_lib/msim/fifo_generator_v13_2_2
vmap lib_fifo_v1_0_11 modelsim_lib/msim/lib_fifo_v1_0_11
vmap axi_datamover_v5_1_19 modelsim_lib/msim/axi_datamover_v5_1_19
vmap axi_sg_v4_1_10 modelsim_lib/msim/axi_sg_v4_1_10
vmap axi_cdma_v4_1_17 modelsim_lib/msim/axi_cdma_v4_1_17
vmap proc_sys_reset_v5_0_12 modelsim_lib/msim/proc_sys_reset_v5_0_12
vmap generic_baseblocks_v2_1_0 modelsim_lib/msim/generic_baseblocks_v2_1_0
vmap axi_infrastructure_v1_1_0 modelsim_lib/msim/axi_infrastructure_v1_1_0
vmap axi_register_slice_v2_1_17 modelsim_lib/msim/axi_register_slice_v2_1_17
vmap axi_data_fifo_v2_1_16 modelsim_lib/msim/axi_data_fifo_v2_1_16
vmap axi_crossbar_v2_1_18 modelsim_lib/msim/axi_crossbar_v2_1_18
vmap axi_protocol_converter_v2_1_17 modelsim_lib/msim/axi_protocol_converter_v2_1_17
vmap axi_clock_converter_v2_1_16 modelsim_lib/msim/axi_clock_converter_v2_1_16
vmap axi_dwidth_converter_v2_1_17 modelsim_lib/msim/axi_dwidth_converter_v2_1_17

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"/opt/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/opt/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/opt/Xilinx/Vivado/2018.2/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work axi_lite_ipif_v3_0_4 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/cced/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work lib_cdc_v1_0_2 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work lib_pkg_v1_0_2 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/0513/hdl/lib_pkg_v1_0_rfs.vhd" \

vcom -work axi_timer_v2_0_19 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/0a2c/hdl/axi_timer_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_axi_timer_0_0/sim/design_1_axi_timer_0_0.vhd" \

vcom -work lib_srl_fifo_v1_0_2 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/51ce/hdl/lib_srl_fifo_v1_0_rfs.vhd" \

vcom -work axi_uartlite_v2_0_21 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/a15e/hdl/axi_uartlite_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_axi_uartlite_0_0/sim/design_1_axi_uartlite_0_0.vhd" \

vlog -work blk_mem_gen_v8_3_6 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/2751/simulation/blk_mem_gen_v8_3.v" \

vcom -work axi_bram_ctrl_v4_0_14 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/6db1/hdl/axi_bram_ctrl_v4_0_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_axi_bram_ctrl_0_0/sim/design_1_axi_bram_ctrl_0_0.vhd" \

vlog -work blk_mem_gen_v8_4_1 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/67d8/simulation/blk_mem_gen_v8_4.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../bd/design_1/ip/design_1_blk_mem_gen_0_0/sim/design_1_blk_mem_gen_0_0.v" \

vlog -work fifo_generator_v13_2_2 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/7aff/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_2 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_2 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.v" \

vcom -work lib_fifo_v1_0_11 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/6078/hdl/lib_fifo_v1_0_rfs.vhd" \

vcom -work axi_datamover_v5_1_19 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec8a/hdl/axi_datamover_v5_1_vh_rfs.vhd" \

vcom -work axi_sg_v4_1_10 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/6e5f/hdl/axi_sg_v4_1_rfs.vhd" \

vcom -work axi_cdma_v4_1_17 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/1403/hdl/axi_cdma_v4_1_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_axi_cdma_0_0/sim/design_1_axi_cdma_0_0.vhd" \

vcom -work proc_sys_reset_v5_0_12 -64 -93 \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/f86a/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_proc_sys_reset_0_0/sim/design_1_proc_sys_reset_0_0.vhd" \

vlog -work generic_baseblocks_v2_1_0 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_infrastructure_v1_1_0 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_17 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/6020/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_data_fifo_v2_1_16 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/247d/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_18 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/15a3/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \

vlog -work axi_protocol_converter_v2_1_17 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ccfb/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work axi_clock_converter_v2_1_16 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/e9a5/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work axi_dwidth_converter_v2_1_17 -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/2839/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../LES5_AXI_DMA_BLOCK_DESIGN.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" \
"../../../bd/design_1/ip/design_1_auto_ds_4/sim/design_1_auto_ds_4.v" \
"../../../bd/design_1/ip/design_1_auto_pc_2/sim/design_1_auto_pc_2.v" \
"../../../bd/design_1/ip/design_1_auto_ds_3/sim/design_1_auto_ds_3.v" \
"../../../bd/design_1/ip/design_1_auto_ds_2/sim/design_1_auto_ds_2.v" \
"../../../bd/design_1/ip/design_1_auto_pc_1/sim/design_1_auto_pc_1.v" \
"../../../bd/design_1/ip/design_1_auto_ds_1/sim/design_1_auto_ds_1.v" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \
"../../../bd/design_1/ip/design_1_auto_ds_0/sim/design_1_auto_ds_0.v" \
"../../../bd/design_1/ip/design_1_s01_regslice_0/sim/design_1_s01_regslice_0.v" \
"../../../bd/design_1/ip/design_1_auto_us_0/sim/design_1_auto_us_0.v" \
"../../../bd/design_1/ip/design_1_auto_rs_w_0/sim/design_1_auto_rs_w_0.v" \
"../../../bd/design_1/ip/design_1_s00_regslice_0/sim/design_1_s00_regslice_0.v" \
"../../../bd/design_1/ip/design_1_auto_us_df_0/sim/design_1_auto_us_df_0.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/sim/design_1.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

