onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+design_1 -L xil_defaultlib -L xpm -L axi_lite_ipif_v3_0_4 -L lib_cdc_v1_0_2 -L lib_pkg_v1_0_2 -L axi_timer_v2_0_19 -L lib_srl_fifo_v1_0_2 -L axi_uartlite_v2_0_21 -L blk_mem_gen_v8_3_6 -L axi_bram_ctrl_v4_0_14 -L blk_mem_gen_v8_4_1 -L fifo_generator_v13_2_2 -L lib_fifo_v1_0_11 -L axi_datamover_v5_1_19 -L axi_sg_v4_1_10 -L axi_cdma_v4_1_17 -L proc_sys_reset_v5_0_12 -L generic_baseblocks_v2_1_0 -L axi_infrastructure_v1_1_0 -L axi_register_slice_v2_1_17 -L axi_data_fifo_v2_1_16 -L axi_crossbar_v2_1_18 -L axi_protocol_converter_v2_1_17 -L axi_clock_converter_v2_1_16 -L axi_dwidth_converter_v2_1_17 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.design_1 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {design_1.udo}

run -all

endsim

quit -force
